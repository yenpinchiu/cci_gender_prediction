name=gender_prediction
deploy_dir=/srv/$(name)
staging_dir=/srv/stage/$(name)
venv_dir=/srv/miniconda3/envs/$(name)
venv_pip=$(venv_dir)/bin/pip
venv_python=$(venv_dir)/bin/python
version=$(shell ./bin/sbt -no-colors version|tail -1|awk '{print $$2}')

ifdef SPARK_HOST
    # rsync to remote host when $SPARK_HOST is set
    SRVHOST_PATH=$(SPARK_HOST):$(deploy_dir)
    STAGEHOST_PATH=$(SPARK_HOST):$(staging_dir)
    SYNC_CMD=rsync
    spark_host=$(SPARK_HOST)
else
    # rsync to local /srv when $SPARK_HOST is not set
    SRVHOST_PATH=$(deploy_dir)
    STAGEHOST_PATH=$(staging_dir)
    SYNC_CMD=sudo rsync
    spark_host=master-general.spark.appier.info
endif

# get nfs user, only useful on old masters (e.g. ara)
# Note: Some operations could only be executed when the
# owner of nfs is not `nobody`.
# For example, installing any previously unseen packages
# via conda rather than pip.
# I.e., they can only be executed on old masters (e.g., ara).
# TODO: remove this when no old masters exist
ifeq ($(shell test -e /srv/miniconda3 && echo -n yes),yes)
    nfs_user=$(shell ls -ld /srv/miniconda3 | cut -f3 -d' ')
else
    nfs_user=root
endif
ifeq ($(nfs_user), nobody)
    nfs_user=root
endif

# compile the jars
build:
	./bin/sbt assembly

ci:
	./bin/sbt ~assembly

# publish the jars
pub:
	./bin/sbt publish

# run unit tests
test:
	./bin/sbt test

# remove artifacts during build
clean:
	./bin/sbt clean

# install to production location
install:
	$(SYNC_CMD) -rlptv --progress . $(SRVHOST_PATH) --include-from include-list.txt --exclude-from exclude-list.txt

# install to staging location
stage:
	$(SYNC_CMD) -rlptv --progress . $(STAGEHOST_PATH) --include-from include-list.txt --exclude-from exclude-list.txt

# set up virtualenv in local /srv
# this assumes miniconda3 have been installed already
setup: | $(venv_dir)
	echo "install packages..."
	sudo -H $(venv_pip) install --upgrade pip
	if [ -e $(HOME)/.ssh/id_rsa ] ; then \
	    sudo -H ssh-agent sh -c 'ssh-add $(HOME)/.ssh/id_rsa; GIT_SSH_COMMAND="ssh -i $(HOME)/.ssh/id_rsa" $(venv_pip) install -r requirements.txt' ; \
	else \
	    sudo -H $(venv_pip) install -r requirements.txt ; \
	fi

$(venv_dir): /srv/miniconda3/bin/conda
	sudo -u $(nfs_user) -H /srv/miniconda3/bin/conda create -n $(name) python=3.6

# set up virtualenv in remote $SPARK_HOST via ansible
# this assumes miniconda3 have been installed already
remote_setup:
	ansible-playbook \
	    -i $(spark_host), \
	    -u root \
	    --private-key ~/.ssh/Spark6-user.pem \
	    --extra-vars "project_name=$(name)" \
	    ansible/setup.yml

# install jars to remote $SPARK_HOST via ansible
remote_install:
	ansible-playbook \
	    -i $(spark_host), \
	    -u root \
	    --private-key ~/.ssh/Spark6-user.pem \
	    --extra-vars "project_name=$(name)" \
	    --extra-vars "jar_dir=target/scala-2.11" \
	    --extra-vars "jar_name=$(name)-assembly-$(version).jar" \
	    ansible/install.yml
