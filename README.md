Example Spark Project
======================

## bootstrap (only for first time)
```bash
$ git clone git@bitbucket.org:plaxieappier/gender_prediction.git
$ mv gender_prediction your_project_name
$ cd your_project_name
$ ./bootstrap
```

This would modify the project to be under the `your_project_name` namespace.
In addition, it modifies git config to point to `https://bitbucket.org/plaxieappier/your_project_name`.

## Set Up Python Virtual Environment
Modify `requirements.txt` and execute
```bash
$ make setup
```

This would install a virtual environment under `/srv/miniconda3/envs/your_project_name`.

## Compile and Deploy

At spark master machines such as master-general.spark.appier.info

    git clone https://bitbucket.org/plaxieappier/gender_prediction
    cd gender_prediction
    make build # compile
    make install # deploy

The jars would be installed at `/srv/gender_prediction`.

    make stage # deploy to staging location

The jars would be installed at `/srv/stage/gender_prediction`.

## Remote Operations

1. Set `$SPARK_HOST` environment variable as the remote spark host.
2. Put Spark6-user.pem into `~/.ssh/`.

The following operations would automatically change the destination to `/srv` on the remote host:

    make install
    make stage

In addition, you could use the following operations to setup & install via ansible:

    make remote_setup # set up virtual environment on remote host via ansible
    make remote_install # deploy to remote host via ansible

## Configure Scalafmt Code Formatting

Create a `.scalafmt.conf` at the root directory of the project. E.g.,

    style = defaultWithAlign
    align.openParenCallSite = false
    align.openParenDefnSite = false
    align.tokens = [{code = "->"}, {code = "<-"}, {code = "=>", owner = "Case"}]
    continuationIndent.callSite = 2
    continuationIndent.defnSite = 2
    danglingParentheses = true
    indentOperator = spray
    maxColumn = 120
    newlines.alwaysBeforeTopLevelStatements = true
    project.excludeFilters = [".*\\.sbt"]
    rewrite.rules = [RedundantParens, SortImports]
    spaces.inImportCurlyBraces = false
    unindentTopLevelOperators = true