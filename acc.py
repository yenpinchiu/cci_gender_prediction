f_groundtruth = open("part-all.test", 'r', encoding='utf-8')
groundtruth_label = []
for line in f_groundtruth:
    groundtruth_label.append(int(line.split(" ")[0]))
f_groundtruth.close()

f_predict = open("part-all.test.out", 'r', encoding='utf-8')
predict_label = []
for line in f_predict:
    predict_label.append(float(line[:-1]))
f_predict.close()

total = 0
pos = 0
for i in range(0, len(groundtruth_label)):
    if predict_label[i] >= 0.5 and groundtruth_label[i] == 1:
        pos += 1
    
    if predict_label[i] < 0.5 and groundtruth_label[i] == 0:
        pos += 1

    if predict_label[i] >= 0.5 or predict_label[i] <= 0.5:
        total +=1

print(float(pos)/total, pos , total)
