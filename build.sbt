name := "gender_prediction"

version := "0.1-spark-2.2.0-SNAPSHOT"

organization := "com.appier"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.2.0" % "provided"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.0" % "provided"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.11"
libraryDependencies += "org.rogach" %% "scallop" % "2.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"

libraryDependencies += "com.appier" %% "cd_object" % "0.2-spark-2.2.0-SNAPSHOT"

publishMavenStyle := true

resolvers += "snapshots" at "http://maven.spark.appier.info:8080/repository/snapshots/"
resolvers += "releases" at "http://maven.spark.appier.info:8080/repository/releases/"

publishTo := (version.value.trim.endsWith("SNAPSHOT") match {
  case true =>
    Some(
      "snapshots" at "http://maven.spark.appier.info:8080/repositories/snapshots")
  case false =>
    Some(
      "releases" at "http://maven.spark.appier.info:8080/repositories/releases")
})

// make sbt test work
parallelExecution in Test := false

scalafmtOnCompile in ThisBuild := true
