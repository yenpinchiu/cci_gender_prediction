#!/srv/miniconda3/envs/gender_prediction/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

from cron_utils.scheduler import submit_job as remote_submit
from cron_utils.scheduler import default_parser as remote_parser
from cron_utils.scheduler import spark_classpath as remote_classpath


def spark_classpath(stage=False):
    return remote_classpath(
        project='gender_prediction',
        stage=stage,
        package='gender_prediction',
        scala_version='2.11',
        version='0.1-spark-2.2.0-SNAPSHOT')


def local_class(cls):
    return 'com.appier.gender_prediction.' + cls


def submit_job(class_name,
               job_name,
               title,
               status,
               priority,
               retry,
               cmd_args='',
               args=None,
               inputs=None,
               outputs=None,
               host=None,
               pool=None,
               group=None,
               stage=False):
    classpath = spark_classpath(stage)

    remote_submit(
        classpath,
        class_name,
        job_name,
        title,
        status,
        priority,
        retry,
        cmd_args,
        args,
        inputs,
        outputs,
        host,
        pool,
        group,
        timeout=8 * 60 * 60)


def default_parser(host='localhost', group='gender_prediction'):
    return remote_parser(
        host=host, status='pending', priority=51, retry=1, pool=256, group=group)
