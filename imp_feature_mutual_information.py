f_in = open("part-all", "r", encoding="utf-8")

his = {}
total = [0, 0]
count = 0
for line in f_in:
    features = line[1:-1].split(" :|: ")
    label = line[0]

    for feature in features:
        if feature not in his:
            his.update({feature: [0, 0]})
        if label == "0":
            his[feature][0] += 1
            total[0] += 1
        elif label == "1":
            his[feature][1] += 1
            total[1] += 1

    count += 1
    if count % 200000 == 0:
        print(count)

f_in.close()

f_male = open("male_feature_mu", "w", encoding="utf-8")
f_female = open("female_feature_mu", "w", encoding="utf-8")
f_all = open("all_feature_mu", "w", encoding="utf-8")

feature_mu = {}
for feature in his:
    if his[feature][0] + his[feature][1] >= 100:
        male_mu = his[feature][0]/((his[feature][0] + his[feature][1]) * total[0])
        female_mu = his[feature][1]/((his[feature][0] + his[feature][1]) * total[1])
        all_mu = male_mu * his[feature][0] + female_mu * his[feature][1]
        feature_mu.update({feature: (
                male_mu, 
                female_mu, 
                all_mu, 
                his[feature][0], 
                his[feature][1], 
                male_mu/female_mu if female_mu != 0 else "inf"
            )})

for item in sorted(feature_mu.items(), key=lambda kv: kv[1][0], reverse=True):
    f_male.write(str(item) + "\n")

for item in sorted(feature_mu.items(), key=lambda kv: kv[1][1], reverse=True):
    f_female.write(str(item) + "\n")

for item in sorted(feature_mu.items(), key=lambda kv: kv[1][2], reverse=True):
    f_all.write(str(item) + "\n")

f_male.close()
f_female.close()
f_all.close()
