package com.appier.gender_prediction

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

object CCIImpCoverage {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
    object conf extends ScallopConf(args) {
      val title = opt[String]()
      verify()
    }

    val optTitle = conf.title.toOption
    val title = (Option(taskName) ++ optTitle).mkString(" ")

    val sparkConf = JobUtil.defaultConf(title)
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._
    
    val cci = spark.read.parquet(
        List(
          "s3a://appier-cd-imp/out/ext_dmp/date=20180725/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180726/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180727/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180728/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180729/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180730/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180731/country=jp/restricted=no"
        ): _*)
      .filter(col("adid") !== "DNT")
      .select(
        $"adid", 
        $"gender"
      )
      .groupBy($"adid")
      .agg(
        collect_list($"gender").alias("gender")
      )
      .select(
        $"adid", 
        udfGenderMerge($"gender").alias("gender")
      )
    
    cci.show(100, false)
    println(cci.rdd.count)

    val imp = spark.read.parquet(
        List(
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180725/country=jp", 
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180726/country=jp", 
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180727/country=jp", 
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180728/country=jp", 
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180729/country=jp", 
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180730/country=jp", 
          "s3a://appier-cd-imp/out/imp_fill_ip/date=20180731/country=jp"
        ): _*)
      .groupBy($"adid")
      .count()
    
    imp.show(100, false)
    println(imp.rdd.count)

    val imp_cci_join = imp
    .join(cci, imp("adid") === cci("adid"))
    
    imp_cci_join.show(100, false)
    println(imp_cci_join.rdd.count)

    val imp_cci_join_labeled = imp_cci_join
    .filter(length($"gender") > 0)

    imp_cci_join_labeled.show(100, false)
    println(imp_cci_join_labeled.rdd.count) 

  }

  def genderHis(gender_list: Seq[String]): Array[Int] = {
    var gender_his = Array[Int](0, 0)

    for(gender <- gender_list) {
      if(gender != null && !gender.isEmpty()){
        if(gender == "m")
          gender_his(0) += 1
        else if(gender == "f")
          gender_his(1) += 1 
      } 
    }
    return gender_his
  }

  val udfGenderMerge = udf(genderMerge _)
  def genderMerge(gender_list: Seq[String]): String = {
    var gender_his = genderHis(gender_list)

    if(gender_his(0) == 0 && gender_his(1) == 0)
      ""
    else if(gender_his(0) >= gender_his(1))
      "m"
    else
      "f"
  }
}
