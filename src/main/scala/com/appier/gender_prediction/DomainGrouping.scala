package com.appier.gender_prediction

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

import java.net.URL
import java.net.MalformedURLException

object DomainGrouping {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
    object conf extends ScallopConf(args) {
      val title = opt[String]()
      verify()
    }

    val optTitle = conf.title.toOption
    val title = (Option(taskName) ++ optTitle).mkString(" ")

    val sparkConf = JobUtil.defaultConf(title)
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._
    
    val cci_label = spark.read
      .parquet(
        List(
          "s3a://appier-cd-imp/out/ext_dmp/date=20180725/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180726/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180727/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180728/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180729/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180730/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180731/country=jp/restricted=no"
        ): _*)
      .select($"page", $"adid", $"gender", $"age")
      .withColumn("page", udfGetHost($"page"))
      .groupBy($"page", $"adid")
      .agg(
          collect_list($"gender").alias("gender"), 
          collect_list($"age").alias("age")
        )
      .withColumn("gender", udfGenderDef($"gender"))
      .withColumn("age", udfAgeDef($"age"))
      .groupBy($"page")
      .agg(
          collect_list($"adid").alias("adid"), 
          collect_list($"gender").alias("gender"), 
          collect_list($"age").alias("age")
        )
      .withColumn("gender", udfGenderHis($"gender"))
      .withColumn("female_ratio", udfGetFemaleRatio($"gender"))
      .withColumn("age", udfAgeHis($"age"))
      .withColumn("age_ratio", udfAgeRatio($"age"))
      .withColumn("count", udfGetCount($"adid"))
      .drop($"adid")
      .sort($"count".desc)
      .rdd
    
    for (data_row <- cci_label.collect())
      println(data_row)
  }

  val udfGenderHis = udf(genderHis _)
  def genderHis(gender_list: Seq[String]): Array[Int] = {
    var gender_his = Array[Int](0, 0, 0)

    for(gender <- gender_list) {
      if(gender != null && !gender.isEmpty()){
        if(gender == "m")
          gender_his(0) += 1
        else if(gender == "f")
          gender_his(1) += 1 
      } else {
        gender_his(2) += 1 
      }
    }
    return gender_his
  }

  val udfGenderDef = udf(genderDef _)
  def genderDef(gender_list: Seq[String]): String = {
    var gender_his = genderHis(gender_list)

    if(gender_his(0)!=0)
      "m"
    else if(gender_his(1)!=0)
      "f"
    else
      ""
  }

  val udfAgeDef = udf(ageDef _)
  def ageDef(age_list: Seq[String]): Double = {
    var age_sum = 0.0
    var count = 0

    for(age <- age_list) {
      if(age != null && !age.isEmpty()){
        age_sum += age.toDouble
        count += 1
      }
    }

    if (count != 0)
      age_sum / count //take average
    else
      0
  }

  val udfAgeHis = udf(ageHis _)
  def ageHis(age_list: Seq[Double]): Array[Double] = {
    var age_his = Array[Double](0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    
    for(age <- age_list) {
      if (age == 0.0)
        age_his(0) += 1.0
      else if (18 <= age && age < 25)
        age_his(1) += 1.0
      else if (25 <= age && age < 35)
        age_his(2) += 1.0
      else if (35 <= age && age < 45)
        age_his(3) += 1.0
      else if (45 <= age && age < 55)
        age_his(4) += 1.0
      else if (55 <= age && age < 65)
        age_his(5) += 1.0
      else if (65 <= age)
        age_his(6) += 1.0
    }

    age_his
  }

  val udfAgeRatio = udf(ageRatio _)
  def ageRatio(age_his: Seq[Double]): Array[Double] = {
    val total_count = age_his(1) + age_his(2) + age_his(3) + age_his(4) + age_his(5) + age_his(6)

    if (total_count != 0)
      Array[Double](
        age_his(1)/total_count, 
        age_his(2)/total_count, 
        age_his(3)/total_count, 
        age_his(4)/total_count, 
        age_his(5)/total_count, 
        age_his(6)/total_count
      )
    else
      Array[Double](0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
  }

  val udfGetHost = udf(getHost _)
  def getHost(url: String): String = {
    try {
      new URL(url).getHost
    } catch {
      case e: MalformedURLException =>  "domain_extract_fail"  
    }
  }

  val udfGetCount = udf(getCount _)
  def getCount(adid_list: Seq[String]): Int = {
    var count = 0
    for(adid <- adid_list) {
      if(adid != null && !adid.isEmpty()){
        count += 1
      }
    }

    count
  }

  val udfGetFemaleRatio = udf(getFemaleRatio _)
  def getFemaleRatio(gender_his: Seq[Int]): Double = {
    val total_labeled = gender_his(0) + gender_his(1)
    if (total_labeled != 0)
      gender_his(1).toDouble/total_labeled
    else
      0.0
  }
}