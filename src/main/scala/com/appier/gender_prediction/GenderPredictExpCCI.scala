package com.appier.gender_prediction

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

import java.net.URL
import java.net.MalformedURLException

import scala.util.hashing.MurmurHash3

//import org.json4s._
//import org.json4s.jackson.JsonMethods._

object GenderPredictExpCCI {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
    object conf extends ScallopConf(args) {
      verify()
    }
    val title = taskName

    val sparkConf = JobUtil.defaultConf(title)
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._

    val cci_train_dataset = spark.read
      .parquet(
        List(
          "s3a://appier-cd-imp/out/ext_dmp/date=20180725/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180726/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180727/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180728/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180729/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180730/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180731/country=jp/restricted=no"
        ): _*)
        .filter(col("adid") !== "DNT")
        .filter(length($"gender") > 0)
        .select(
          $"adid", 
          $"gender", 
          udfGenerateLocationFeature($"region_code", $"city", $"city_gnid", $"zip", $"isp").alias("location_features"), 
          udfGenerateDeviceFeature($"os", $"device_type", $"app_type", $"osv", $"device").alias("device_features"), 
          udfGeneratePageFeature($"page").alias("page_features"), 
          udfGenerateKeyWordFeature($"keywords").alias("keyword_features"), 
          udfGenerateAgeFeature($"age").alias("age_features")
        )
        .groupBy($"adid")
        .agg(
          collect_list($"gender").alias("gender"),
          collect_list($"keyword_features").alias("keyword_features"),
          collect_list($"location_features").alias("location_features"),
          collect_list($"device_features").alias("device_features"),
          collect_list($"page_features").alias("page_features"),
          collect_list($"age_features").alias("age_features")
        )
        .select(
          $"adid", 
          udfGenderMerge($"gender").alias("gender"), 
          udfFeatureHash(
            udfFeatureMerge(
              $"keyword_features", 
              $"location_features", 
              $"device_features", 
              $"page_features", 
              $"age_features"
            ), 
            lit(60000000)
          )
          .alias("features")
        )
        .select(
          udfFFMFormat($"gender", $"features").alias("ffm_data_str")
        )
        .rdd
        .map(x=>x.mkString(""))
        .saveAsTextFile("s3a://appier-cd-imp/tmp/cci_ffm/")
  }

  def getHost(url: String): String = {
    try {
      new URL(url).getHost
    } catch {
      case e: MalformedURLException =>  "domain_extract_fail"  
    }
  }

  val udfGenerateAgeFeature = udf(generateAgeFeature _)
  def generateAgeFeature(age: String): Seq[String] = {
    val age_f = "age:" + age.toInt / 5 // 5歲一個bin

    Seq[String](age_f)
  }

  val udfGeneratePageFeature = udf(generatePageFeature _)
  def generatePageFeature(page: String): Seq[String] = {
    val page_f = "page:" + getHost(page)

    Seq[String](page_f)
  }

  val udfGenerateDeviceFeature = udf(generateDeviceFeature _)
  def generateDeviceFeature(os: String, device_type: String, app_type: String,
   osv: String, device: String): Seq[String] = {
    val os_f = "os:" + os
    val device_type_f = "device_type:" + device_type
    val app_type_f = "app_type:" + app_type
    val osv_device_f = "osv_device:" + device + osv // 單獨osv存在沒意義,所以和device綁定(理論上如果是跑ffm可以不用綁,反正會做interaction,但考量到通用性還是獨立出來)
    val device_f = "device:" + device // device單獨獨立出來一個

    Seq[String](os_f, device_type_f, app_type_f, osv_device_f, device_f)
  }

  val udfGenerateLocationFeature = udf(generateLocationFeature _)
  def generateLocationFeature(region_code: String, city: String, 
  city_gnid: String, zip: String, isp: String): Seq[String] = {
    //region_code, city, city_gnid, zip這幾個基本上都是地區相關,看起來幾乎是1對1,理論上只要加一個,但保險起見反正都加
    val region_code_f = "region_code:" + region_code
    val city_f = "city:" + city
    val city_gnid_f = "city_gnid:" + city_gnid
    val zip_f = "zip:" + zip
    val isp_f = "isp:" + isp

    Seq[String](region_code_f, city_f, city_gnid_f, zip_f, isp_f)
  }

  val udfGenerateKeyWordFeature = udf(generateKeyWordFeature _)
  def generateKeyWordFeature(keywords: String): Seq[String] = {
    parseKeywords(keywords)
  }

  def parseKeywords(keywords: String): Seq[String] = {
    /*
    implicit val formats = DefaultFormats
    try {
      parse(keywords).extract[Seq[String]]
    } catch {
      //case e: com.fasterxml.jackson.core.JsonParseException =>  Seq[String]()
      case e: Exception => Seq[String]() //其實可能的jon parse錯誤也不少,直接全接吧
    }
    */
    //json parse問題一大堆,又要去",又要去換行,去"還要不能去掉json本身的",乾脆自己切吧
    keywords
    .replace("[", "")
    .replace("]", "")
    .replace("\"", "")
    .replace("\n", "")
    .split(",")
  }

  def HashUInt(input_str: String, size: Int): Int = {
    MurmurHash3.stringHash(input_str) & 0x7FFFFFFF % size
  }

  val udfFeatureMerge = udf(featureMerge _)
  def featureMerge(keyword_features: Seq[Seq[String]], 
  location_features: Seq[Seq[String]],
  device_features: Seq[Seq[String]],
  page_features: Seq[Seq[String]],
  age_features: Seq[Seq[String]]): Seq[String] = {
    val all_f = keyword_features ++ 
    location_features ++ 
    device_features ++ 
    page_features ++ 
    age_features

    all_f.flatten.distinct
  }

  val udfFeatureHash = udf(featureHash _)
  def featureHash(features: Seq[String], size: Int): Seq[Int] = {
    features.map(x => HashUInt(x, size))
  }

  def genderHis(gender_list: Seq[String]): Array[Int] = {
    var gender_his = Array[Int](0, 0)

    for(gender <- gender_list) {
      if(gender != null && !gender.isEmpty()){
        if(gender == "m")
          gender_his(0) += 1
        else if(gender == "f")
          gender_his(1) += 1 
      } 
    }
    return gender_his
  }

  val udfGenderMerge = udf(genderMerge _)
  def genderMerge(gender_list: Seq[String]): String = {
    var gender_his = genderHis(gender_list)

    if(gender_his(0) == 0 && gender_his(1) == 0)
      ""
    else if(gender_his(0) >= gender_his(1))
      "m"
    else
      "f"
  }

  val udfFFMFormat = udf(ffmformat _)
  def ffmformat(gender: String, features_hash: Seq[Int]): String = {
    val label = if (gender == "m") 0 else 1
    var ffm_format_data = label.toString

    val features_hash_sorted = scala.util.Sorting.stableSort(features_hash)
    
    for (f <- features_hash_sorted){
      ffm_format_data += " 0:" + f + ":1"
    }

    ffm_format_data
  }
}