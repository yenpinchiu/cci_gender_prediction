package com.appier.gender_prediction

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

import scala.util.hashing.MurmurHash3

import java.util.Date
import java.text.SimpleDateFormat

import java.net.URL
import java.net.MalformedURLException

//這邊目標是弄個no collision版本
//因為如果全部在spark上跑的話,要在個機器間keep一個index表很麻煩
//所以採用其他做法做法,就是寫的時候寫沒hash的出去
//抓下來cat完後再單機用個python script跑一遍做一對一的index
//其實單機跑python script處理280多萬筆資料不用五分鐘,蠻快的
//python script在train_data_format.py
//並且,這種事後處理的方式對於feature組合測試或改變處理方式還都蠻方便的,產生一份資料之後就可以改train_data_format.py就好
object GenderPredictExpImpNoCollision {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
    object conf extends ScallopConf(args) {
      val title = opt[String]()
      verify()
    }

    val optTitle = conf.title.toOption
    val title = (Option(taskName) ++ optTitle).mkString(" ")

    val sparkConf = JobUtil.defaultConf(title)
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._

    val cci_label = spark.read.parquet(
      List(
        "s3a://appier-cd-imp/out/ext_dmp/date=20180725/country=jp/restricted=no", 
        "s3a://appier-cd-imp/out/ext_dmp/date=20180726/country=jp/restricted=no", 
        "s3a://appier-cd-imp/out/ext_dmp/date=20180727/country=jp/restricted=no", 
        "s3a://appier-cd-imp/out/ext_dmp/date=20180728/country=jp/restricted=no", 
        "s3a://appier-cd-imp/out/ext_dmp/date=20180729/country=jp/restricted=no", 
        "s3a://appier-cd-imp/out/ext_dmp/date=20180730/country=jp/restricted=no", 
        "s3a://appier-cd-imp/out/ext_dmp/date=20180731/country=jp/restricted=no"
      ): _*)
      .filter(col("adid") !== "DNT")
      .select(
        udfAdidNoPostfix($"adid").alias("cci_adid"), 
        $"gender"
      )
      .filter(length($"gender") > 0)
      .groupBy($"cci_adid")
      .agg(
        collect_list($"gender").alias("gender")
      )
      .select(
        $"cci_adid", 
        udfGenderMerge($"gender").alias("gender")
      )

    val implog = spark.read.parquet(
      List(
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180725/country=jp", 
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180726/country=jp", 
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180727/country=jp", 
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180728/country=jp", 
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180729/country=jp", 
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180730/country=jp", 
        "s3a://appier-cd-imp/out/imp_fill_ip/date=20180731/country=jp"
      ): _*)
      .select(
        udfAdidNoPostfix($"adid").alias("imp_adid"), 
        $"partner_id", 
        $"app_type", 
        $"device_type", 
        $"os", 
        $"gender".alias("imp_gender"), //幾乎完全沒有
        $"age", //幾乎完全沒有
        $"current_age", //幾乎完全沒有
        $"__app_name", 
        $"app_bundle", 
        udfAttrInteraction($"os", $"osv").alias("osv"), 
        $"device", 
        $"lat", 
        $"lon", 
        $"city", 
        $"zip", 
        $"city_gnid", 
        $"isp", 
        $"region_code", 
        $"lang", 
        $"web_host", 
        $"page", 
        $"keywords", 
        $"time", 
        $"detected_vertical", 
        $"categories"
      )

    val implog_f = implog
      .join(cci_label, implog("imp_adid") === cci_label("cci_adid"))
      .select(
        $"imp_adid", 
        udfGenerateGeneralFeature($"partner_id", lit("partner_id:")).alias("partner_id_f"), 
        udfGenerateGeneralFeature($"app_type", lit("app_type:")).alias("app_type_f"), 
        udfGenerateGeneralFeature($"device_type", lit("device_type:")).alias("device_type_f"), 
        udfGenerateGeneralFeature($"os", lit("os:")).alias("os_f"), 
        udfGenerateGeneralFeature($"imp_gender", lit("imp_gender:")).alias("imp_gender_f"), 
        udfGenerateAgeFeature($"age", lit("age:")).alias("age_f"), 
        udfGenerateAgeFeature($"current_age", lit("current_age:")).alias("current_age_f"), 
        udfGenerateGeneralFeature($"__app_name", lit("__app_name:")).alias("__app_name_f"), 
        udfGenerateGeneralFeature($"app_bundle", lit("app_bundle:")).alias("app_bundle_f"), 
        udfGenerateGeneralFeature($"device", lit("device:")).alias("device_f"), 
        udfGenerateGeneralFeature($"osv", lit("osv:")).alias("osv_f"), 
        udfGenerateLocFeature($"lat", lit("lat:")).alias("lat_f"), 
        udfGenerateLocFeature($"lon", lit("lon:")).alias("lon_f"), 
        udfGenerateGeneralFeature($"city", lit("city:")).alias("city_f"), 
        udfGenerateGeneralFeature($"zip", lit("zip:")).alias("zip_f"), 
        udfGenerateGeneralFeature($"city_gnid", lit("city_gnid:")).alias("city_gnid_f"), 
        udfGenerateGeneralFeature($"isp", lit("isp:")).alias("isp_f"), 
        udfGenerateGeneralFeature($"region_code", lit("region_code:")).alias("region_code_f"), 
        udfGenerateGeneralFeature($"lang", lit("lang:")).alias("lang_f"), 
        udfGenerateGeneralFeature($"web_host", lit("web_host:")).alias("web_host_f"), 
        udfGeneratePageFeature($"page", lit("page:")).alias("page_f"), 
        udfGenerateGeneralFeature($"keywords", lit("keywords:")).alias("keywords_f"), 
        udfGenerateTimeFeature($"time", lit("time:")).alias("time_f"), 
        udfGenerateDetectedVerticalFeature($"detected_vertical", lit("detected_vertical:")).alias("detected_vertical_f"), 
        udfGenerateCategoriesFeature($"categories", lit("categories:")).alias("categories_f")
      )
      .select(
        $"imp_adid", 
        udfFlattenSeq(array($"partner_id_f", $"app_type_f", $"device_type_f", $"os_f", $"imp_gender_f", $"__app_name_f", $"app_bundle_f", 
          $"age_f", $"current_age_f", 
          $"device_f", $"osv_f", $"lat_f", $"lon_f", $"city_f", $"zip_f", $"city_gnid_f", $"isp_f", $"region_code_f", 
          $"lang_f", $"web_host_f", $"page_f", $"keywords_f", $"time_f", $"detected_vertical_f", $"categories_f"
        )).alias("imp_f")
      )
      .as[(String, Seq[String])]
      .rdd
      .map(x => (x._1, itemHis(x._2)))
      .reduceByKey((x,y) => mapMerge(x, y))
      .toDF("imp_adid", "implog_f")

    //Ad Value
    val ad_value = spark.read.parquet(
        "s3a://appier-cd-attribute/out/merge_ad_value/device/date=20180731"
      )
      .select($"id".alias("ad_value_adid"), $"category")
      .groupBy($"ad_value_adid")
      .agg(
        collect_list($"category").alias("category")
      )

    val ad_value_f = ad_value
      .select(
        $"ad_value_adid", 
        udfGenerateSeqFeature($"category", lit("ad_value_category:")).alias("category_f")
      )
      .select(
        $"ad_value_adid", 
        udfFlattenSeq(array($"category_f")).alias("ad_value_f")
      )
      .select(
        $"ad_value_adid", 
        udfItemHis($"ad_value_f").alias("ad_value_f")
      )

    //Aixon Device Keyword
    val aixon_device_keyword = spark.read.text(
        "s3a://projectx-userlist-bucket/user_history/production/jp/topic/20180730/impjoinall/topic_data/"
      )
      .select(udfExtractAixonKeywordAdid($"value").alias("aixon_device_keyword_adid"), udfExtractAixonKeywordKeywords($"value").alias("keywords"))

    val aixon_device_keyword_f = aixon_device_keyword
      .select(
        $"aixon_device_keyword_adid", 
        udfGenerateSeqFeature($"keywords", lit("aixon_device_keyword_keywords:")).alias("keywords_f")
      )
      .select(
        $"aixon_device_keyword_adid", 
        udfFlattenSeq(array($"keywords_f")).alias("aixon_device_keyword_f")
      )
      .select(
        $"aixon_device_keyword_adid", 
        udfItemHis($"aixon_device_keyword_f").alias("aixon_device_keyword_f")
      )

    //"DeviceActiveTime_v0_1"
    val device_activetime = spark.read.parquet(
        "s3a://appier-cd-attribute/out/device_active_time/version=0.1/date=20180729/country=jp"
      )
      .select($"id".alias("device_activetime_id"), $"distribution", $"active_24hours", $"active_weekday", $"active_168hours", $"home_or_work")

    val device_activetime_f = device_activetime
      .select(
        $"device_activetime_id", 
        udfGenerateDADistibutionFeature($"distribution", lit("dadis:")).alias("distribution_f"), 
        udfGenerateIntLongMapFeature($"active_24hours", lit("da24:")).alias("active_24hours_f"), 
        udfGenerateIntLongMapFeature($"active_weekday", lit("daw:")).alias("active_weekday_f"), 
        udfGenerateIntLongMapFeature($"active_168hours", lit("da168:")).alias("active_168hours_f"), 
        when($"home_or_work".isNull, udfCreteEmptyArray()).otherwise(udfGenerateBooleanFeature($"home_or_work", lit("dhow:"))).alias("home_or_work_f")
      )
      .select(
        $"device_activetime_id", 
        udfFlattenSeq(array($"distribution_f", $"active_24hours_f", $"active_weekday_f", 
        $"active_168hours_f", $"home_or_work_f")).alias("device_activetime_f")
      )
      .select(
        $"device_activetime_id", 
        udfItemHis($"device_activetime_f").alias("device_activetime_f")
      )

    //"DeviceAPPInfo_v0_1"
    //只能join到7百多個,應該沒啥屁用
    val device_app_info = spark.read.parquet(
        "s3a://appier-cd-attribute/out/device_app_info/version=0.1/date=20180731/country=jp"
      )
      .select(
        $"id".alias("device_app_info_id"), 
        $"ios_app_count", 
        $"ios_primary_genre_count", 
        $"ios_genre_count", 
        $"ios_notfree_count", 
        $"ios_age_rating_count", 
        $"android_app_count", 
        $"android_genre_count", 
        $"android_notfree_count"
      )

    val device_app_info_f = device_app_info
      .select(
        $"device_app_info_id", 
        udfGenerateLongLongMapFeature($"ios_app_count", lit("ios_app_count:")).alias("ios_app_count_f"), 
        udfGenerateIntLongMapFeature($"ios_primary_genre_count", lit("ios_primary_genre_count:")).alias("ios_primary_genre_count_f"), 
        udfGenerateIntLongMapFeature($"ios_genre_count", lit("ios_genre_count:")).alias("ios_genre_count_f"), 
        udfGenerateBoolLongMapFeature($"ios_notfree_count", lit("ios_notfree_count:")).alias("ios_notfree_count_f"), 
        udfGenerateStringLongMapFeature($"ios_age_rating_count", lit("ios_age_rating_count:")).alias("ios_age_rating_count_f"), 
        udfGenerateStringLongMapFeature($"android_app_count", lit("android_app_count:")).alias("android_app_count_f"), 
        udfGenerateStringLongMapFeature($"android_genre_count", lit("android_genre_count:")).alias("android_genre_count_f"), 
        udfGenerateBoolLongMapFeature($"android_notfree_count", lit("android_notfree_count:")).alias("android_notfree_count_f")
      )
      .select(
        $"device_app_info_id", 
        udfFlattenSeq(array($"ios_app_count_f", $"ios_primary_genre_count_f", $"ios_genre_count_f", 
        $"ios_notfree_count_f", $"ios_age_rating_count_f", $"android_app_count_f", $"android_genre_count_f", 
        $"android_notfree_count_f")).alias("device_app_info_id_f")
      )
      .select(
        $"device_app_info_id", 
        udfItemHis($"device_app_info_id_f").alias("device_app_info_id_f")
      )

    //"DeviceCampaignActivity_v0_2"
    val device_camp_act = spark.read.parquet(
        "s3a://appier-cd-attribute/out/device_campaign_activity/version=0.2/date=20180729/country=all/"
      )
      .select(
        $"id".alias("device_camp_act_id"), 
        $"group_show_count", 
        $"group_click_count", 
        $"group_show_and_click_count", 
        $"group_purchase_amount", 
        $"group_app_purchase_amount", 
        $"group_web_purchase_amount"
      )

    val device_camp_act_f = device_camp_act
      .select(
        $"device_camp_act_id", 
        udfGenerateStringIntMapFeature($"group_show_count", lit("group_show_count:")).alias("group_show_count_f"), 
        udfGenerateStringIntMapFeature($"group_click_count", lit("group_click_count:")).alias("group_click_count_f"), 
        udfGenerateStringIntMapFeature($"group_show_and_click_count", lit("group_show_and_click_count:")).alias("group_show_and_click_count_f"), 
        udfGenerateStringDoubleMapFeature($"group_purchase_amount", lit("group_purchase_amount:")).alias("group_purchase_amount_f"), 
        udfGenerateStringDoubleMapFeature($"group_app_purchase_amount", lit("group_app_purchase_amount:")).alias("group_app_purchase_amount_f"), 
        udfGenerateStringDoubleMapFeature($"group_web_purchase_amount", lit("group_web_purchase_amount:")).alias("group_web_purchase_amount_f")
      )
      .select(
        $"device_camp_act_id", 
        udfFlattenSeq(array($"group_show_count_f", $"group_click_count_f", $"group_show_and_click_count_f", 
        $"group_purchase_amount_f", $"group_app_purchase_amount_f", $"group_web_purchase_amount_f")).alias("device_camp_act_f")
      )
      .select(
        $"device_camp_act_id", 
        udfItemHis($"device_camp_act_f").alias("device_camp_act_f")
      )

    val joined_f = implog_f
      .join(cci_label, implog_f("imp_adid") === cci_label("cci_adid")) //join label
      .join(ad_value_f, implog_f("imp_adid") === ad_value_f("ad_value_adid"), "left_outer")
      .join(aixon_device_keyword_f, implog_f("imp_adid") === aixon_device_keyword_f("aixon_device_keyword_adid"), "left_outer")
      .join(device_activetime_f, implog_f("imp_adid") === device_activetime_f("device_activetime_id"), "left_outer")
      .join(device_camp_act_f, implog_f("imp_adid") === device_camp_act_f("device_camp_act_id"), "left_outer")
      .join(device_app_info_f, implog_f("imp_adid") === device_app_info_f("device_app_info_id"), "left_outer")
      .select(
        $"imp_adid".alias("adid"), 
        $"gender", 
        udfMapMergeSeq(array(
            $"implog_f", 
            $"ad_value_f", 
            $"aixon_device_keyword_f", 
            $"device_activetime_f", 
            $"device_camp_act_f", 
            $"device_app_info_id_f"
          )
        ).alias("features")
      )

    val ffm_data = joined_f
      .as[(String, String, Map[String, Int])]
      .rdd
      .map(x => ffmformat(x._2, x._3))
      .saveAsTextFile("s3a://appier-cd-imp/tmp/cci_ffm/")

    //for (data_row <- ffm_data.take(1000))
    //  println(data_row)
  }

  //val array_ = udf(() => Array.empty[Int])
  val udfCreteEmptyArray = udf(creteEmptyArray _)
  def creteEmptyArray(): Seq[String] = {
    Seq[String]()
  }

  def ffmformat(gender: String, features: Map[String, Int]): String = {
    "%d %s" format (
      if (gender == "m") 0 else 1, 
      scala.util.Sorting.stableSort(
        features
        .map{case (k, v) => k}.toSeq
      )
      .map(x => "%s |:| %d" format (x, features(x)))
      mkString " :|: "
    )
  }

  val udfExtractAixonKeywordAdid = udf(extractAixonKeywordAdid _)
  def extractAixonKeywordAdid(str: String): String = {
    str.split("\t")(0)
  }

  val udfExtractAixonKeywordKeywords = udf(extractAixonKeywordKeywords _)
  def extractAixonKeywordKeywords(str: String): Seq[String] = {
    str.split("\t")(1).split(',')
  }

  val udfAdidNoPostfix = udf(adidNoPostfix _)
  def adidNoPostfix(adid: String): String = {
    adid.split(":")(1)
  }

  val udfMapMergeSeq = udf(mapMergeSeq _)
  def mapMergeSeq(maps: Seq[Map[String, Int]]): Map[String, Int] = {
    maps.reduceLeft((r, m) => mapMerge(r, m))
  }

  def mapMerge(map1: Map[String, Int], map2: Map[String, Int]): Map[String, Int] = {
    if (map1 != null && map2 != null){
      map1 ++ map2.map{ case (k,v) => k -> (v + map1.getOrElse(k,0)) }
    }
    else if (map1 != null){
      map1
    }
    else{
      map2
    }
  }

  val udfItemHis = udf(itemHis _)
  def itemHis(item_list: Seq[String]): Map[String, Int] = {
      item_list.filter(_.size != 0).groupBy(identity).mapValues(_.size) //有去掉空的string
  }

  val udfFlattenSeq = udf(flattenSeq _)
  def flattenSeq(seq: Seq[Seq[String]]): Seq[String] = {
    seq.flatten
  }

  val udfAttrInteraction = udf(attrInteraction _)
  def attrInteraction(attr_1: String, attr_2: String): String = {
    attr_1 + " " + attr_2
  }

  val udfGenderMerge = udf(genderMerge _)
  def genderMerge(gender_list: Seq[String]): String = {
    itemHis(gender_list).maxBy { case (key, value) => value }
    ._1
  }

  val udfGenerateGeneralFeature = udf(generateGeneralFeature _)
  def generateGeneralFeature(attr: String, postfix: String): Seq[String] = {
    if(attr != null && !attr.isEmpty()){
      Seq[String](postfix + attr)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateAgeFeature = udf(generateAgeFeature _)
  def generateAgeFeature(attr: String, postfix: String): Seq[String] = {
    if(attr != null && !attr.isEmpty()){
      Seq[String](postfix + attr.toInt / 6) //六年一個bin
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateTimeFeature = udf(generateTimeFeature _)
  def generateTimeFeature(timestamp: String, postfix: String): Seq[String] = {
    if(timestamp != null && !timestamp.isEmpty()){
      val time_str_split = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss")
      .format(timestamp.toLong * 1000L).split("-")

      //年感覺沒什麼用
      Seq[String](
        postfix + "MM:" + time_str_split(1), 
        postfix + "dd:" + time_str_split(2), 
        postfix + "HH:" + time_str_split(3)
      )
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateCategoriesFeature = udf(generateCategoriesFeature _)
  def generateCategoriesFeature(categories: String, postfix: String): Seq[String] = {
    if(categories != null && !categories.isEmpty()){
      categories
      .replace("[", "")
      .replace("]", "")
      .replace("\"", "")
      .split(",")
      .map(x => postfix + x)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateDetectedVerticalFeature = udf(generateDetectedVerticalFeature _)
  def generateDetectedVerticalFeature(detected_vertical: String, postfix: String): Seq[String] = {
    if(detected_vertical != null && !detected_vertical.isEmpty()){
      detected_vertical
      .replace("[", "")
      .replace("]", "")
      .replace("\"", "")
      .split("},")
      .map(x => x.replace("{", "").replace("}", ""))
      .map(x => x.split(",")
        .map(y => y.split(":")(1))
      )
      .map(x => postfix + x(0))
    }
    else{
      Seq[String]()
    } 
  }

  val udfGeneratePageFeature = udf(generatePageFeature _)
  def generatePageFeature(page: String, postfix: String): Seq[String] = {
    if(page != null && !page.isEmpty()){
      Seq[String](
        postfix + "url:" + page, 
        postfix + "domain:" + getHost(page)
      )
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateLocFeature = udf(generateLocFeature _)
  def generateLocFeature(loc: String, postfix: String): Seq[String] = {
    if(loc != null && !loc.isEmpty()){
      Seq[String](postfix + (loc.toDouble / 0.1).toInt)
    }
    else{
      Seq[String]()
    }
  }

  def getHost(url: String): String = {
    try {
      new URL(url).getHost
    } catch {
      case e: MalformedURLException =>  "domain_extract_fail"  
    }
  }

  val udfGenerateSeqFeature = udf(generateSeqFeature _)
  def generateSeqFeature(attrs: Seq[String], postfix: String): Seq[String] = {
    attrs
    .map(x => postfix + x)
  }

  val udfGenerateBooleanFeature = udf(generateBooleanFeature _)
  def generateBooleanFeature(attr: Boolean, postfix: String): Seq[String] = {
    //這邊,不需要檢查attr是不是null,因為scala中boolean根本不nullable,String和Map等都需要檢查
    //如果是null,則spark根本不會讓他進這個udf,會直接回傳null
    Seq[String](postfix + (if (attr) "1" else "0"))
  }

  val udfGenerateIntLongMapFeature = udf(generateIntLongMapFeature _)
  def generateIntLongMapFeature(attrs: Map[Int, Long], postfix: String): Seq[String] = {
    if (attrs != null){
      attrs
      .map{case (k, v) => k}
      .toSeq
      .map(x => postfix + x)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateStringIntMapFeature = udf(generateStringIntMapFeature _)
  def generateStringIntMapFeature(attrs: Map[String, Int], postfix: String): Seq[String] = {
    if (attrs != null){
      attrs
      .map{case (k, v) => k}
      .toSeq
      .map(x => postfix + x)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateStringDoubleMapFeature = udf(generateStringDoubleMapFeature _)
  def generateStringDoubleMapFeature(attrs: Map[String, Double], postfix: String): Seq[String] = {
    if (attrs != null){
      attrs
      .map{case (k, v) => k}
      .toSeq
      .map(x => postfix + x)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateLongLongMapFeature = udf(generateLongLongMapFeature _)
  def generateLongLongMapFeature(attrs: Map[Long, Long], postfix: String): Seq[String] = {
    if (attrs != null){
      attrs
      .map{case (k, v) => k}
      .toSeq
      .map(x => postfix + x)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateBoolLongMapFeature = udf(generateBoolLongMapFeature _)
  def generateBoolLongMapFeature(attrs: Map[Boolean, Long], postfix: String): Seq[String] = {
    if (attrs != null){
      attrs
      .map{case (k, v) => k}
      .toSeq
      .map(x => postfix + x.toString)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateStringLongMapFeature = udf(generateStringLongMapFeature _)
  def generateStringLongMapFeature(attrs: Map[String, Long], postfix: String): Seq[String] = {
    if (attrs != null){
      attrs
      .map{case (k, v) => k}
      .toSeq
      .map(x => postfix + x)
    }
    else{
      Seq[String]()
    }
  }

  val udfGenerateDADistibutionFeature = udf(generateDADistibutionFeature _)
  def generateDADistibutionFeature(attrs: Map[Int, Long], postfix: String): Seq[String] = {
    if (attrs != null){
      val max_count = attrs.maxBy(_._2)._2
      if (max_count <= 1){
        Seq[String](postfix + "0")
      }
      else if (max_count <= 5){
        Seq[String](postfix + "1")
      }
      else if (max_count <= 10){
        Seq[String](postfix + "2")
      }
      else if (max_count <= 15){
        Seq[String](postfix + "3")
      }
      else if (max_count <= 20){
        Seq[String](postfix + "4")
      }
      else{
        Seq[String](postfix + "5")
      }
    }
    else{
      Seq[String]()
    }
  }
}
