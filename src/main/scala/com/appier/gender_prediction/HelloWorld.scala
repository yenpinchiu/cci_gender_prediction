package com.appier.gender_prediction

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

object HelloWorld {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
    object conf extends ScallopConf(args) {
      val name = opt[String](required = true)
      val outfile = opt[String](required = true)
      val npart = opt[Int](default = Option(640))
      val title = opt[String]()
      verify()
    }

    val name = conf.name()
    val outfile = conf.outfile()
    val npart = conf.npart()
    val optTitle = conf.title.toOption

    val title = (Option(taskName) ++ optTitle).mkString(" ")

    val sparkConf = JobUtil.defaultConf(title)
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._

    spark
      .createDataFrame(Seq(Tuple1("Hello %s!".format(name))))
      .repartition(npart)
      .write
      .option("compression", "gzip")
      .mode(SaveMode.Overwrite)
      .text(outfile)
  }
}
