package com.appier.gender_prediction

import org.apache.log4j.LogManager
import org.apache.spark.storage.StorageLevel._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SaveMode

import org.rogach.scallop._

import org.apache.spark.sql.functions._
import org.apache.spark.sql._

case class Atomic[T, V](key: T, value: V)
case class LabelData(device_id: String, birth: Seq[Atomic[(Int, Int), Int]], gender: Seq[Atomic[Int, Int]], timestamp: Long)

object SourceCompare {
  val logger = LogManager.getLogger(this.getClass.getSimpleName)

  val taskName = this.getClass.getSimpleName.init

  def main(args: Array[String]) {
    object conf extends ScallopConf(args) {
      val title = opt[String]()
      verify()
    }

    val optTitle = conf.title.toOption
    val title = (Option(taskName) ++ optTitle).mkString(" ")

    val sparkConf = JobUtil.defaultConf(title)
    val spark = SparkSession
      .builder()
      .config(sparkConf)
      .getOrCreate()
    import spark.implicits._

    val unified_device_id = spark.read.parquet("s3a://appier-cd-sdid/out/unified_device_id_map_cache/d2u/date=20180731/country=jp")
    
    val cci_label = spark.read.parquet(
        List(
          "s3a://appier-cd-imp/out/ext_dmp/date=20180725/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180726/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180727/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180728/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180729/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180730/country=jp/restricted=no", 
          "s3a://appier-cd-imp/out/ext_dmp/date=20180731/country=jp/restricted=no"
        ): _*)
      .select($"adid".alias("cci_adid"), $"gender".alias("cci_gender"), $"age".alias("cci_age"))
      .filter(col("cci_adid") !== "DNT")
      .withColumn("cci_adid", udfAdidNoPostfix($"cci_adid"))
      .join(unified_device_id, $"cci_adid" === unified_device_id("id"), "left_outer")
      .withColumn("cci_compound_id", udfCompoundID($"cci_adid", $"unifyid"))
      .groupBy($"cci_compound_id")
      .agg(
          collect_list($"cci_gender").alias("cci_gender"), 
          collect_list($"cci_age").alias("cci_age"),
          collect_list($"cci_adid").alias("cci_adid"), 
          collect_list($"unifyid").alias("cci_unifyid"), 
          collect_list($"id").alias("cci_id")
        )
      .withColumn("cci_gender", udfCountHis($"cci_gender"))
      .withColumn("cci_age", udfCountHis($"cci_age"))
      .withColumn("cci_adid", udfCountHis($"cci_adid"))
      .withColumn("cci_unifyid", udfCountHis($"cci_unifyid"))
      .withColumn("cci_id", udfCountHis($"cci_id"))

    val eyeota_label = spark.read.parquet("s3a://appier-cd-persona/input/gender_age/label/date=20180731/country=jp/source=eyeota")
      .select($"device_id".alias("eyeota_device_id"), $"birth".alias("eyeota_birth"), $"gender".alias("eyeota_gender"))
      .join(unified_device_id, $"eyeota_device_id" === unified_device_id("id"), "left_outer")
      .withColumn("eyeota_compound_id", udfCompoundID($"eyeota_device_id", $"unifyid"))
      .groupBy($"eyeota_compound_id")
      .agg(
          collect_list($"unifyid").alias("eyeota_unifyid"), 
          collect_list($"id").alias("eyeota_id"), 
          collect_list($"eyeota_device_id").alias("eyeota_device_id"), 
          collect_list($"eyeota_birth").alias("eyeota_birth"), 
          collect_list($"eyeota_gender").alias("eyeota_gender")
        )
      .withColumn("eyeota_device_id", udfCountHis($"eyeota_device_id"))
      .withColumn("eyeota_unifyid", udfCountHis($"eyeota_unifyid"))
      .withColumn("eyeota_id", udfCountHis($"eyeota_id"))

    val eyeota_cci_join_label = cci_label
      .join(eyeota_label, cci_label("cci_compound_id") === eyeota_label("eyeota_compound_id"), "left_outer")
      .select($"cci_compound_id", $"cci_age", $"cci_gender", $"eyeota_compound_id", $"eyeota_birth", $"eyeota_gender")

    val eyeota_cci_join_label_rdd = eyeota_cci_join_label
      .as[(String, Map[String, Int], Map[String, Int], String, Seq[Seq[Atomic[(Int, Int), Int]]], Seq[Seq[Atomic[Int, Int]]])]
      .rdd
      .map(x => sourceCondition(x))
      .reduce((x, y) => conditionCountReduce(x, y))

    print(eyeota_cci_join_label_rdd)

    /*
    val app_site_label = spark.read.parquet("s3a://appier-cd-persona/input/gender_age/label/date=20180731/country=jp/source=app_site_history_custom_white1")
      .select($"device_id".alias("app_site_device_id"), $"birth".alias("app_site_birth"), $"gender".alias("app_site_gender"))
      .join(unified_device_id, $"app_site_device_id" === unified_device_id("id"), "left_outer")
      .withColumn("app_site_compound_id", udfCompoundID($"app_site_device_id", $"unifyid"))
      .groupBy($"app_site_compound_id")
      .agg(
          collect_list($"unifyid").alias("app_site_unifyid"), 
          collect_list($"id").alias("app_site_id"), 
          collect_list($"app_site_device_id").alias("app_site_device_id"), 
          collect_list($"app_site_birth").alias("app_site_birth"), 
          collect_list($"app_site_gender").alias("app_site_gender")
        )
      .withColumn("app_site_device_id", udfCountHis($"app_site_device_id"))
      .withColumn("app_site_unifyid", udfCountHis($"app_site_unifyid"))
      .withColumn("app_site_id", udfCountHis($"app_site_id"))

    val app_site_cci_join_label = cci_label
      .join(app_site_label, cci_label("cci_compound_id") === app_site_label("app_site_compound_id"), "left_outer")
      .select($"cci_compound_id", $"cci_age", $"cci_gender", $"app_site_compound_id", $"app_site_birth", $"app_site_gender")

    val app_site_cci_join_label_rdd = app_site_cci_join_label
      .as[(String, Map[String, Int], Map[String, Int], String, Seq[Seq[Atomic[(Int, Int), Int]]], Seq[Seq[Atomic[Int, Int]]])]
      .map(x => sourceCondition(x))
      .reduce((x, y) => conditionCountReduce(x, y))

    print(app_site_cci_join_label_rdd)
    */
    /*
    val merge_bid_label = spark.read.parquet("s3a://appier-cd-persona/input/gender_age/label/date=20180731/country=jp/source=merge_bid_truth")
      .select($"device_id".alias("merge_bid_device_id"), $"birth".alias("merge_bid_birth"), $"gender".alias("merge_bid_gender"))
      .join(unified_device_id, $"merge_bid_device_id" === unified_device_id("id"), "left_outer")
      .withColumn("merge_bid_compound_id", udfCompoundID($"merge_bid_device_id", $"unifyid"))
      .groupBy($"merge_bid_compound_id")
      .agg(
          collect_list($"unifyid").alias("merge_bid_unifyid"), 
          collect_list($"id").alias("merge_bid_id"), 
          collect_list($"merge_bid_device_id").alias("merge_bid_device_id"), 
          collect_list($"merge_bid_birth").alias("merge_bid_birth"), 
          collect_list($"merge_bid_gender").alias("merge_bid_gender")
        )
      .withColumn("merge_bid_device_id", udfCountHis($"merge_bid_device_id"))
      .withColumn("merge_bid_unifyid", udfCountHis($"merge_bid_unifyid"))
      .withColumn("merge_bid_id", udfCountHis($"merge_bid_id"))

    val merge_bid_cci_join_label = cci_label
      .join(merge_bid_label, cci_label("cci_compound_id") === merge_bid_label("merge_bid_compound_id"), "left_outer")
      .select($"cci_compound_id", $"cci_age", $"cci_gender", $"merge_bid_compound_id", $"merge_bid_birth", $"merge_bid_gender")

    val merge_bid_cci_join_label_rdd = merge_bid_cci_join_label
      .as[(String, Map[String, Int], Map[String, Int], String, Seq[Seq[Atomic[(Int, Int), Int]]], Seq[Seq[Atomic[Int, Int]]])]
      .rdd
      .map(x => sourceCondition(x))
      .reduce((x, y) => conditionCountReduce(x, y))

    print(merge_bid_cci_join_label_rdd)
    */
    /*
    val spark5_label = spark.read.parquet("s3a://appier-cd-persona/input/gender_age/label/date=20180731/country=jp/source=spark5.de")
      //.as[LabelData]
      .select($"device_id".alias("spark5_device_id"), $"birth".alias("spark5_birth"), $"gender".alias("spark5_gender"))
      .join(unified_device_id, $"spark5_device_id" === unified_device_id("id"), "left_outer")
      .withColumn("spark5_compound_id", udfCompoundID($"spark5_device_id", $"unifyid"))
      .groupBy($"spark5_compound_id")
      .agg(
          collect_list($"unifyid").alias("spark5_unifyid"), 
          collect_list($"id").alias("spark5_id"), 
          collect_list($"spark5_device_id").alias("spark5_device_id"), 
          collect_list($"spark5_birth").alias("spark5_birth"), 
          collect_list($"spark5_gender").alias("spark5_gender")
        )
      .withColumn("spark5_device_id", udfCountHis($"spark5_device_id"))
      .withColumn("spark5_unifyid", udfCountHis($"spark5_unifyid"))
      .withColumn("spark5_id", udfCountHis($"spark5_id"))

    val spark5_cci_join_label = cci_label
      .join(spark5_label, cci_label("cci_compound_id") === spark5_label("spark5_compound_id"), "left_outer")
      .select($"cci_compound_id", $"cci_age", $"cci_gender", $"spark5_compound_id", $"spark5_birth", $"spark5_gender")

    val spark5_cci_join_label_rdd = spark5_cci_join_label
      .as[(String, Map[String, Int], Map[String, Int], String, Seq[Seq[Atomic[(Int, Int), Int]]], Seq[Seq[Atomic[Int, Int]]])]
      .rdd
      .map(x => sourceCondition(x))
      .reduce((x, y) => conditionCountReduce(x, y))

    print(spark5_cci_join_label_rdd)
    */

    /*
    for (data_row <- eyeota_cci_join_label_rdd.collect())
      println(data_row)
    */
  }

  val udfAdidNoPostfix = udf(adidNoPostfix _)
  def adidNoPostfix(adid: String): String = {
    adid.split(":")(1)
  }

  val udfCountHis = udf(countHis _)
  def countHis(item_list: Seq[String]): scala.collection.mutable.Map[String, Int] = {
    var item_his = scala.collection.mutable.Map[String, Int]()

    for(item <- item_list) {
      if(item != null && !item.isEmpty()){
        if(!item_his.contains(item)){
          item_his += (item -> 0)
        }
        item_his(item) += 1
      }
    }
    return item_his
  }

  val udfCompoundID = udf(compoundID _)
  def compoundID(id: String, unifyid: String): String = {
    if(unifyid != null){
      unifyid
    }
    else {
      id
    }
  }

    def sourceCondition(x: (
        String, 
        Map[String, Int], 
        Map[String, Int], 
        String, 
        Seq[Seq[Atomic[(Int, Int), Int]]], 
        Seq[Seq[Atomic[Int, Int]]])): 
        (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int)
    = {
        var total_count = 0
        var match_count = 0
        var age_gain_count = 0
        var age_loss_count = 0
        var age_match_count = 0
        var age_match_correct_count = 0
        var gender_gain_count = 0
        var gender_loss_count = 0
        var gender_match_count = 0
        var gender_match_correct_count = 0

        total_count = 1
        if(x._4 != null){
            match_count = 1

            var has_age = false
            for(r <- x._5){
                if(r.size != 0)
                    has_age = true
            }

            var has_gender = false
            for(r <- x._6){
                if(r.size != 0)
                    has_gender = true
            }

            if(x._2.size != 0 && has_age){
                age_match_count = 1

                age_match_correct_count = 1
                for ((cci_age,v) <- x._2){
                    val cci_year = 2018 - cci_age.toInt
                    var correct = false
                    for(r <- x._5){
                        for(rr <- r){
                            if (cci_year.toInt >= rr.key._1 && cci_year.toInt <= rr.key._2)
                                correct = true
                        }
                    }
                    
                    if(!correct)
                        age_match_correct_count = 0
                }
            } 

            if(x._2.size == 0 && has_age){
                age_loss_count = 1
            } 

            if(x._2.size != 0 && !has_age){
                age_gain_count = 1
            }

            if(x._3.size != 0 && has_gender){
                gender_match_count = 1

                gender_match_correct_count = 1
                for ((cci_gender,v) <- x._3){
                    var correct = false
                    for(r <- x._6){
                        for(rr <- r){
                            if ((rr.key == 1 && cci_gender == "m") || (rr.key == 0 && cci_gender == "f"))
                                correct = true
                        }
                    }

                    if(!correct)
                        gender_match_correct_count = 0
                }
            } 

            if(x._3.size == 0 && has_gender){
                gender_loss_count = 1
            } 

            if(x._3.size != 0 && !has_gender){
                gender_gain_count = 1
            } 
        }
        else{
            if(x._2.size != 0)
                age_gain_count = 1
            
            if(x._3.size != 0)
                gender_gain_count = 1
        }

        (
            total_count, 
            match_count, 
            age_gain_count, 
            age_loss_count, 
            age_match_count, 
            age_match_correct_count, 
            gender_gain_count, 
            gender_loss_count, 
            gender_match_count, 
            gender_match_correct_count
        )
    }

    def conditionCountReduce(
        x: (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int),
        y: (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int)
    ): (Int, Int, Int, Int, Int, Int, Int, Int, Int, Int)
    = {
        (
          x._1 + y._1, 
          x._2 + y._2, 
          x._3 + y._3, 
          x._4 + y._4, 
          x._5 + y._5, 
          x._6 + y._6, 
          x._7 + y._7, 
          x._8 + y._8, 
          x._9 + y._9, 
          x._10 + y._10 
        )
    }
}
