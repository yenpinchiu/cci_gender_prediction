#!/srv/miniconda3/envs/gender_prediction/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals, absolute_import, division, print_function

from cron_utils.args import generate_args
from cron_utils.args import sub

from common import default_parser, local_class, submit_job

def main(host, status, priority, retry, pool, group, stage, script):

    file_name = script
    class_name = local_class(file_name)
    job_name = sub('[gender_prediction] {file_name}')

    inputs = []
    outputs = []
    args = dict(group=group)
    title = script
    cmd_args = generate_args()

    submit_job(
        class_name,
        job_name,
        title,
        status,
        priority,
        retry,
        inputs=inputs,
        outputs=outputs,
        host=host,
        cmd_args=cmd_args,
        args=args,
        pool=pool,
        group=group,
        stage=stage)

if __name__ == '__main__':
    parser = default_parser()
    parser.add_argument('--script', required=True)
    args = parser.parse_args()

    main(
        host=args.host,
        status=args.status,
        priority=args.priority,
        retry=args.retry,
        pool=args.pool,
        group=args.group,
        stage=args.stage, 
        script=args.script
    )
