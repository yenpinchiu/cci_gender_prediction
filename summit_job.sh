#!/bin/bash

SCRIPT=${1} # "process" or "merge"

eval "$(ssh-agent -s)"
sudo ssh-add ~/.ssh/id_rsa

make build
make install

virtualenv manually_summit_env
source manually_summit_env/bin/activate
pip install -r requirements.txt
python summit_job.py --script $SCRIPT
deactivate
rm -rf manually_summit_env

pkill ssh-agent