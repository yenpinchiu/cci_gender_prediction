f_in = open("part-all", "r", encoding="utf-8")

f_his = {}
for line in f_in:
    label = line[0]
    fs = line[2:-1].split(" :|: ")

    for ff in fs:
        f = ff.split(" |:| ")[0]
        if f not in f_his:
            f_his.update({f: 0})
        f_his[f] += 1

f_in.close()

f_in = open("part-all", "r", encoding="utf-8")
f_out = open("part-all-ffm", "w", encoding="utf-8")

f_dict = {}
f_count = 0
count = 0

for line in f_in:
    label = line[0]
    fs = line[2:-1].split(" :|: ")

    fs_indexed = []
    for ff in fs:
        f = ff.split(" |:| ")[0]
        if f in f_his and f_his[f] > 1:
            if f not in f_dict:
                f_dict.update({f: f_count})
                f_count += 1
            fs_indexed.append(f_dict[f])

    ffm_line = label
    for f_index in sorted(fs_indexed):
        ffm_line += " 0:{}:1".format(f_index)

    f_out.write(ffm_line + "\n")
    
    count += 1
    if count % 10000 == 0:
        print(count)

f_in.close()
f_out.close()
