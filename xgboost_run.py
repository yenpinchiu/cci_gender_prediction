import xgboost as xgb
import random

dtrain = xgb.DMatrix("part-all.train")

param = {'max_depth':3, 'silent':1, 'objective':'binary:logistic'}
num_round = 1000
bst = xgb.train(param, dtrain, num_round)

bst.save_model("gbdt.model")
bst = xgb.Booster(param)
bst.load_model("gbdt.model")

dtest = xgb.DMatrix('part-all.test')
preds = bst.predict(dtest)
test_label = dtest.get_label()

acc = 0
for i in range(0,len(preds)):
    if preds[i] >= 0.5:
        predict_label = 1
    else:
        predict_label = 0

    if predict_label == test_label[i]:
        acc += 1

print(acc)